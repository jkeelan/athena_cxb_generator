# Athena CXB Flux Generator
A short script to generate the cosmic x-ray background differential flux for both Gruber et. al and Moretti et. al spectra.

Outputs cxb_moretti.dat and cxb_gruber.dat.

Header information contains total flux / cm^2 / s and column units.


## Usage
```bash
python cxb_generator.py <emin> <emax>
```
<emin> and <emax> are optional and define the energy limits of the output spectra (inclusive)

## Requirements
Requires numpy.
