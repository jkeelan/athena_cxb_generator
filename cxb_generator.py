import sys
import numpy as np

emin = 50
emax = 10e3
if len(sys.argv) == 3:
    emin = float(sys.argv[1])
    emax = float(sys.argv[2])
print "Output min energy: ", emin, "keV | ", "Output max energy: ", emax, "keV"

#energy cut for output spectrum (keV)


plotting = False

D2_TO_SR = np.power(np.pi/180, 2.0)

## Gruber et. al Constants
# @article{Gruber:1999yr,
#       author         = "Gruber, D. E. and Matteson, J. L. and Peterson, L. E. and
#                         Jung, G. V.",
#       title          = "{The spectrum of diffuse cosmic hard x-rays measured with
#                         heao-1}",
#       journal        = "Astrophys. J.",
#       volume         = "520",
#       year           = "1999",
#       pages          = "124",
#       doi            = "10.1086/307450",
#       eprint         = "astro-ph/9903492",
#       archivePrefix  = "arXiv",
#       primaryClass   = "astro-ph",
#       reportNumber   = "SP-98-25",
#       SLACcitation   = "%%CITATION = ASTRO-PH/9903492;%%"
# }
HIGH_C1 = 0.0259
HIGH_C2 = 0.504
HIGH_C3 = 0.0288

HIGH_POW1 = -5.5
HIGH_POW2 = -1.58
HIGH_POW3 = -1.05

LOW_C1 = 7.877
LOW_EXP1 = 41.13
LOW_POW1 = -0.29


## Moretti et. al Constants
# @article{2012A&A...548A..87M,
#    author = {{Moretti}, A. and {Vattakunnel}, S. and {Tozzi}, P. and {Salvaterra}, R. and 
# 	{Severgnini}, P. and {Fugazza}, D. and {Haardt}, F. and {Gilli}, R.
# 	},
#     title = "{Spectrum of the unresolved cosmic X-ray background: what is unresolved 50 years after its discovery}",
#   journal = {\aap},
# archivePrefix = "arXiv",
#    eprint = {1210.6377},
#  keywords = {X-rays: diffuse background, X-rays: general, galaxies: active},
#      year = 2012,
#     month = dec,
#    volume = 548,
#       eid = {A87},
#     pages = {A87},
#       doi = {10.1051/0004-6361/201219921},
#    adsurl = {http://adsabs.harvard.edu/abs/2012A%26A...548A..87M},
#   adsnote = {Provided by the SAO/NASA Astrophysics Data System}
# }
E_B = 29.0
TAU1 = 1.40
TAU2 = 2.88
PL_NORM = 3.69e-3
CPL_NORM = 3.70e-3
CPL_CUT = 41.13
PL_POW = 1.47
CPL_POW = 1.41
C = 0.109/D2_TO_SR

def cxb_gruber(energies):
    flux = [cxb_background_gruber(e) for e in energies]
    return np.asarray(flux)

def cxb_background_gruber(energy):
    if(energy < 60):
        flux = low_energy_gruber(energy)
    else:
        flux = high_energy_gruber(energy)
    return flux

def low_energy_gruber(energy):
    return LOW_C1*np.exp(-energy/LOW_EXP1)*np.power(energy, LOW_POW1)

def high_energy_gruber(energy):
    e = energy/60
    t1 = HIGH_C1*np.power(e, HIGH_POW1)
    t2 = HIGH_C2*np.power(e, HIGH_POW2)
    t3 = HIGH_C3*np.power(e, HIGH_POW3)
    return t1 + t2 + t3

def cxb_moretti(energies):
    temp = np.asarray([moretti_plaw(e) for e in energies])
    pl = PL_NORM*np.power(energies, -PL_POW)
    cpl = CPL_NORM*np.power(energies, -CPL_POW)
    cpl[energies > CPL_CUT] = 0
    pl[energies > CPL_CUT] = 0

    # ewidth = np.insert(energies, 0, 0)
    # ewidth = (ewidth[1:] - ewidth[:-1])
    # temp = np.cumsum(temp*ewidth)/ewidth
    return (temp*D2_TO_SR)*np.power(energies, 1.0)

def moretti_plaw(energy):
    e = energy/E_B
    den = np.power(e, TAU1) + np.power(e, TAU2)
    num = C
    return num/den



energies = np.logspace(0, 5, 1000)
energies = energies[(energies > emin) & (energies < emax)] #cut energies

#calc values
flux_gruber = cxb_gruber(energies)
flux_moretti = cxb_moretti(energies)

ewidth = (energies[1:] - energies[:-1])

total_pps_gruber = np.sum(flux_gruber[1:]*ewidth/energies[1:])*4*np.pi
total_pps_moretti = np.sum(flux_moretti[1:]*ewidth/energies[1:])*4*np.pi
header_txt_gruber = "Total CXB particles per cm^2 per second Gruber: " + str(total_pps_gruber)
header_txt_moretti = "Total CXB particles per cm^2 per second Moretti: " + str(total_pps_moretti)

print "Spectrum output between", emin, "and", emax, "keV"
print header_txt_gruber
print header_txt_moretti
np.savetxt("cxb_gruber.dat", zip(energies, 4*np.pi*flux_gruber/energies), header=header_txt_gruber+ "| cols: energy (keV)\t count /cm^2/sr/s/keV")
np.savetxt("cxb_moretti.dat", zip(energies, 4*np.pi*flux_moretti/energies), header=header_txt_moretti + "| cols: energy (keV)\t count /cm^2/sr/s/keV")

